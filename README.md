# Packer MacOS and R environment templates

[[_TOC_]]

## MacOS creation and R environment provisioning

If you want to develop and test your R packages, build R packages binaries under/for MacOS, this is the right place.

> __This project is only for developpement and testing.  For other use you must have Apple equipment.__


## What is doing ?

> Create a Vangrant Box with VirtualBox as provider under MacOS Catalina 10.15 with R (>= 4.0.0) and Rtools + some dependencies.

**Boxes are availables here : [https://app.vagrantup.com/VMR/boxes/MacOS_Catalina-R](https://app.vagrantup.com/VMR/boxes/MacOS_Catalina-R)  

## Requirements

* [VirtualBox](https://www.virtualbox.org/) (>= 6.1.14)
* [Vagrant](https://www.vagrantup.com/) (>= 2.2.10)
* [Ansible](https://www.ansible.com/) (>=2.2.1.0)
* sshpass (>= 1.06)

---

* Xcode\_11.4.xip
* Command\_Line\_Tools\_for\_Xcode\_12.dmg
* MacOS\_Catalina.ova

## Current templates

* [packer\_macos.json](main_macos.json)

## Files

* [scripts/](scripts/): packer configuration scripts
* [playbook.yml](playbook.yml): ansible playbook
* [roles/](roles/): ansible roles

## Build Vagrant Box

> edit [build.sh](build.sh) script

Build a Vagrant box with virtualbox and MacOS.

```
packer build --only=virtual-ova packer_macos.json
```

## License

[MIT](LICENSE)

## Authors

* [Jean-Frnaçois Rey](https://gitlab.com/_jfrey_)

## Contributions

All contributions are welcome.

