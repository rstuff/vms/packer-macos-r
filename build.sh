#!/bin/bash -eux

OS="MacOS_Catalina"
VERSION="10.5"
R_VERSION="4.3.1"
HEADLESS=true
ISO="MacOS_Catalina.ova"
BOX_TAG="VMR/${OS}-R"
DESCRIPTION="${OS} + R ${R_VERSION}"

VM_NAME="${OS}-R${R_VERSION}"

usage() { echo "Usage: $0 [-h] [-c <vagrantcloudtoken>]" 1>&2; exit 1; }

while getopts c:h flag
do
    case "${flag}" in
        c) VAGRANTCLOUD_TOKEN=${OPTARG};;
        h) usage;;
    esac
done

packer build \
  -only=virtualbox-ovf \
  -force \
  -var "vm_name=${VM_NAME}" \
  -var "headless=${HEADLESS}" \
	-var "iso_url=${ISO}" \
	-var "input_dir=." \
  -var "boxtag=${BOX_TAG}" \
  -var "vagrantcloud_token=${VAGRANTCLOUD_TOKEN}" \
  -var "version=${R_VERSION}" \
  -var "version_description=${DESCRIPTION}" \
  packer_macos.json 

# for debug mode
#	-debug \
# -on-error=ask \


