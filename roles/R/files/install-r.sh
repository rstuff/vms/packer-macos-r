#!/bin/bash

echo "Downloading gfortran for catalina..."
curl -LO https://github.com/fxcoudert/gfortran-for-macOS/releases/download/10.2/gfortran-10.2-Catalina.dmg
echo "Install gfortran"
hdiutil attach gfortran-10.2-Catalina.dmg
sudo installer -package /Volumes/gfortran*/gfortran.pkg -target /
hdiutil detach /Volumes/gfortran*/
rm gfortran-10.2-Catalina.dmg

#echo "Downloading R-4.2 Nightly..."
#curl -LO https://mac.r-project.org/high-sierra/R-4.2-branch/R-4.2-branch.pkg
#echo "Installing R"
#sudo installer -package R-4.2-branch.pkg -target /
#rm R-4.2-branch.pkg

echo "Downloading R-4.3.1 Stable..."
curl -LO https://cran.r-project.org/bin/macosx/big-sur-x86_64/base/R-4.3.1-x86_64.pkg
echo "Installing R"
sudo installer -package R-4.3.1-x86_64.pk -target /
rm R-4.3.1-x86_64.pkg


echo "export PATH=/usr/local/bin/:/usr/local/gfortran/bin:\$PATH" >> ~/.bashrc
echo "export PATH=/usr/local/bin/:/usr/local/gfortran/bin:\$PATH" >> ~/.zshrc
echo "export PATH=/usr/local/bin/:/usr/local/gfortran/bin:\$PATH" >> ~/.shrc

