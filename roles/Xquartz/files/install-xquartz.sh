#!/bin/bash

echo "Downloading Xquartz"
#curl -OL https://dl.bintray.com/xquartz/downloads/XQuartz-2.8.1.dmg

curl -OL https://github.com/XQuartz/XQuartz/releases/download/XQuartz-2.8.1/XQuartz-2.8.1.dmg
hdiutil attach XQuartz-2.7.11.dmg
sudo installer -package /Volumes/XQuartz-2.8.1/XQuartz.pkg -target /
hdiutil detach /Volumes/XQuartz-2.8.1/
rm XQuartz-2.8.1.dmg
