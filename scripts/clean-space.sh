#!/bin/bash

echo "Clean sleepimage"
sudo pmset -a hibernatemode 0
sudo rm /private/var/vm/sleepimage
sudo touch /private/var/vm/sleepimage
sudo chmod 000 /private/var/vm/sleepimage

echo "Remove tts files"
sudo rm -rf /System/Library/Speech/Voices/*

echo "Remove logs"
sudo rm -rf /private/var/log/*

echo "Clean Cache"
rm -rf /private/var/tmp/TM*
rm -rf ~/Library/Caches/*



