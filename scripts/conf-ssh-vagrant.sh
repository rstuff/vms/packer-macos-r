#!/bin/bash

echo "Change sshd configuration" 
sudo sed -i'' -e "s/#StrictModes yes/StrictModes no/" /etc/ssh/sshd_config
sudo sed -i'' -e "s/#PermitUserEnvironment no/PermitUserEnvironment yes/" /etc/ssh/sshd_config
sudo sed -i'' -e "s/#AllowAgentForwarding yes/AllowAgentForwarding yes/" /etc/ssh/sshd_config


echo "Add vagrant ssh insecure public key"
mkdir -p /Users/vagrant/.ssh
chmod 0700 /Users/vagrant/.ssh
curl -o /Users/vagrant/.ssh/id_rsa.pub https://raw.githubusercontent.com/hashicorp/vagrant/main/keys/vagrant.pub
cat /Users/vagrant/.ssh/id_rsa.pub >> /Users/vagrant/.ssh/authorized_keys
chmod 600 /Users/vagrant/.ssh/authorized_keys
chmod 644 /Users/vagrant/.ssh/id_rsa.pub
chown -R vagrant:staff /Users/vagrant/.ssh

