#!/bin/bash 

VERSION=""
VAGRANTCLOUDTOKEN=""
VM_NAME=""
USER=""

usage() { echo "Usage: $0 [-h] [-c <vagrantcloudtoken>] [-r <version>] [-n <name>] [-u <user>]" 1>&2; exit 1; }

while getopts c:hr:n:u: flag
do
    case "${flag}" in
        c) VAGRANTCLOUDTOKEN=${OPTARG};;
        r) VERSION=${OPTARG};;
        n) VM_NAME=${OPTARG};;
        u) USER=${OPTARG};;
        h) usage;;
    esac
done

if [ $# -lt 4 ]; then
    usage
    exit 1;
fi

TSVERSION=$VERSION.$(date +%s)

echo "# Create a new version $TSVERSION"
curl --header "Content-Type: application/json" \
     --header "Authorization: Bearer $VAGRANTCLOUDTOKEN" \
     "https://app.vagrantup.com/api/v1/box/$USER/$VM_NAME/versions" \
     --data '{ "version": { "version": "'"$TSVERSION"'" } }'

echo "# Create a new provider virtualbox"
curl --header "Content-Type: application/json" \
     --header "Authorization: Bearer $VAGRANTCLOUDTOKEN" \
     "https://app.vagrantup.com/api/v1/box/$USER/$VM_NAME/version/$TSVERSION/providers" \
     --data '{ "provider": { "name": "virtualbox" } }'

echo "# Prepare the provider for upload an upload URL"
response=$(curl \
       	--header "Authorization: Bearer $VAGRANTCLOUDTOKEN" \
       	"https://app.vagrantup.com/api/v1/box/$USER/$VM_NAME/version/$TSVERSION/provider/virtualbox/upload")

echo "# Extract the upload URL"
upload_path=$(echo "$response" | jq .upload_path)

echo "# Upload box"
curl "$upload_path" --request PUT --upload-file "virtualbox_$VM_NAME$VERSION.box"

echo "# Publish release"
curl --header "Authorization: Bearer $VAGRANTCLOUDTOKEN" \
       	"https://app.vagrantup.com/api/v1/box/$USER/$VM_NAME/version/$TSVERSION/release" \
       	--request PUT

echo "# Done"

